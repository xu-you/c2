// node自带的文件路径工具
const path = require('path')
const IS_PROD = ['production', 'prod'].includes(process.env.NODE_ENV);
const BundleAnalyzerPlugin = require('webpack-bundle-analyzer').BundleAnalyzerPlugin;

/**
 * 获得绝对路径
 * @method resolve
 * @param  {String} dir 相对于本文件的路径
 * @return {String}     绝对路径
 */
function resolve (dir) {
    return path.join(__dirname, '..', dir)
}
module.exports = {
  productionSourceMap: false,
  chainWebpack: config => {
    // 添加别名
    config.resolve.alias
      .set('@', resolve('src'))
      .set('assets', resolve('src/assets'))
      .set('components', resolve('src/components'))
      .set('layout', resolve('src/layout'))
      .set('base', resolve('src/base'))
      .set('static', resolve('src/static'));
    // 打包分析
    if (process.env.IS_ANALYZ) {
      config.plugin('webpack-report')
        .use(BundleAnalyzerPlugin, [{
          analyzerMode: 'static',
        }]);
    }
  },
  devServer: {
    port: 8023
  },
  // 默认'/'，部署应用包时的基本 URL  baseUrl: './', // 默认'/'，部署应用包时的基本 URL
  baseUrl: IS_PROD ? process.env.VUE_APP_SRC || "/" : "./",
  configureWebpack: config => {
    config
  }
}