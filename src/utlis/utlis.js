/**
 * 存储 sessionStorage
 */
export const __setItem = (name, content) => {
  if (!name) return;
  if (typeof content !== 'string') {
    content = JSON.stringify(content);
  }
  window.sessionStorage.setItem(name, content);
};

/**
 * 获取 sessionStorage
 */
export const __getItem = name => {
  if (!name) return;
  return window.sessionStorage.getItem(name);
};

/**
 * 删除 sessionStorage
 */
export const __removeItem = name => {
  if (!name) return;
  window.sessionStorage.removeItem(name);
};