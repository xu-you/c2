import Vue from 'vue';
import App from './App.vue';
import router from './router';
import '@babel/polyfill';// 支持低版本的IE
import './plugins/element.js';
import VueI18n from 'vue-i18n';
Vue.use(VueI18n);
/*---------语言包切换-----------*/
const messages = {
  'zh': require('./common/lang/zh'),
  'en': require('./common/lang/en'),
  'us': require('./common/lang/us'),
}
const i18n = new VueI18n({
  locale: 'en',
  messages
})
/*
 *  加载进度条
 *  进度条样式
 */
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
Vue.config.productionTip = false
NProgress.configure({ showSpinner: false }) // 禁用进度条
router.beforeEach((to, from, next) => {
  NProgress.start()
  NProgress.done()
  next()
})

new Vue({
    el: '#app',
    i18n,
    router: router,
    render: h => h(App)
})
