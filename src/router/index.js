import Vue from 'vue'
import Router from 'vue-router'
/**
 * 引入组件
 * @param {pages} view 视图
 * @param {pages} home 首页
 */
const views = () => import('../page/views')
const home = () => import('../page/Home/index')
const newlist = () => import('../page/newlis/index')
Vue.use(Router)
export default new Router({
  routes: [
    {
      path: '/',
      component: views,
      children: [
        { path: '/',
          name: 'home',
          meta: {title: '首页', navshow: false},
          component: home
        },
        { path: '/newlist',
          name: 'newlist',
          meta: {title: '二级', navshow: false},
          component: newlist
        },
      ]
    }
  ]
})
